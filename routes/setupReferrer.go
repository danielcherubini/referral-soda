package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/danmademe/referral-soda/models"
	"github.com/danmademe/referral-soda/utils"
)

func errorResponse(err error, res http.ResponseWriter) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(res, "{\"error\": \"%s\"}", err.Error())
}

//SetupReferrer blah blah blah
func SetupReferrer(res http.ResponseWriter, req *http.Request) {
	referrer := &models.Referrer{}
	decoder := json.NewDecoder(req.Body)
	var referralRequest models.ReferralRequest
	err := decoder.Decode(&referralRequest)
	if err != nil {
		errorResponse(err, res)
	}
	defer req.Body.Close()

	log.Println(referralRequest.Email)

	//Setup struct
	str, err := utils.SecureRandomAlphaString(10)
	referrer.ID = str
	referrer.Email = referralRequest.Email
	referrer.Name = referralRequest.Name
	referrer.DateCreated = time.Now().String()

	if err != nil {
		errorResponse(err, res)
	} else {

		//Marshal responseObject to JsonResponse
		jsonByteArray, _ := json.Marshal(referrer)
		jsonString := string(jsonByteArray)

		res.Header().Set("Content-Type", "application/json")
		res.WriteHeader(http.StatusOK)
		fmt.Fprintf(res, "%s", jsonString)
		fmt.Println(jsonString)
	}
}
