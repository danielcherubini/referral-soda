package models

//Referrer is teh person who referrs
type Referrer struct {
	ID          string     `json:"id"`
	Email       string     `json:"email"`
	Name        string     `json:"name"`
	DateCreated string     `json:"date_created"`
	Referred    []Referred `json:"referred"`
}

//Referred The personwho is referrered
type Referred struct {
	ID          string `json:"id"`
	Email       string `json:"email"`
	Name        string `json:"name"`
	DateCreated string `json:"date_created"`
	ReferrerID  string `json:"referrer_id"`
}

//ReferralRequest is for marshling requests into stuff
type ReferralRequest struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}
