package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/danmademe/referral-soda/routes"
	"github.com/gorilla/mux"
)

//Server setup for server
func Server(r *mux.Router) *http.Server {
	srv := &http.Server{
		Handler: r,
		Addr:    "0.0.0.0:8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	return srv
}

//Setup sets server up for logfatal
func Setup() *http.Server {
	fmt.Println("Starting Router")
	r := routes.Router()
	http.Handle("/", r)
	srv := Server(r)
	return srv
}

func main() {
	log.Fatal(Setup().ListenAndServe())
}
